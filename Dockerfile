FROM thomasleplus/csv 

RUN pip3 install --upgrade httpie

RUN http "https://www.indicepa.gov.it/public-services/opendata-read-service.php?filename=amministrazioni.txt" |  \
    csvjson --tabs --indent 2 > /tmp/amministrazioni.json && \
    echo -e "{\n\"amministrazioni\":\n" > /srv/amministrazioni.json && \
    cat /tmp/amministrazioni.json >> /srv/amministrazioni.json && \
    echo -e "\n}\n" >> /srv/amministrazioni.json && \
    rm /tmp/amministrazioni.json


# TODO: fare un check per validare il risultato
# count_end=$(grep -o '"cod_amm"' amministrazioni.json |wc -l)
# count_begin=$(wc -l amministrazioni.txt)
# ASSERT: $count_end = $count_begin - 1
# (una riga se ne va nell'header)

FROM vimagick/json-server

LABEL org.label-schema.vendor = "Opencontent Scarl" \
      org.label-schema.url="https://indicepa.opencontent.it"

LABEL it.opencontent.autodeploy="true"

COPY --from=0 /srv/amministrazioni.json /srv/data.json

CMD [ "-H", "0.0.0.0", "--watch", "/srv/data.json", "--read-only", "--id", "cod_amm" ]


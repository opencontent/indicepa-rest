# IndicePA ReST
[![pipeline status](https://gitlab.com/opencontent/indicepa-rest/badges/master/pipeline.svg)](https://gitlab.com/opencontent/indicepa-rest/-/commits/master)
Creare una interfaccia ReST per gli OpenData esposti dal sito https://www.indicepa.gov.it/documentale/n-opendata.php

Il servizio creato è disponibile all'indirizzo

  https://indicepa.opencontent.it

## Come si può usare

Alcuni esempi di quali metodi sono supportati

### Semplici GET

	http GET https://indicepa.opencontent.it/amministrazioni?cod_amm=054
	http GET https://indicepa.opencontent.it/amministrazioni?comune=Vicopisano


### Paginazione


	http GET https://indicepa.opencontent.it/amministrazioni?_page=7&_limit=2


### Sorting

	http GET https://indicepa.opencontent.it/amministrazioni?_sort=regione,provincia&_order=asc,asc

### Slice

	http GET https://indicepa.opencontent.it/amministrazioni?_start=20&_end=30

### Full-text search

	http GET https://indicepa.opencontent.it/amministrazioni?q=Landi


